// console.log("Hello World")

let students = [
    
    "John",
    "Jane",
    "Jeff",
    "Kevin",
    "David"

];
console.log(students)

class Dog {

    constructor(name,breed,age){

        this.name = name;
        this.breed = breed;
        this.age = age;
    }

}

let dog1 = new Dog("Bantay","corgi",3);
console.log(dog1)

let person1 = {

    name: "Saitama",
    heroName: "One Punch Man",
    age: 30

}
console.log(person1)

// What array method can we use to add an item at the end of an array?
students.push("Henry");
console.log(students);

// What array method can we use to add an item at the start of an array?
students.unshift("Jeremiah");
console.log(students)

students.unshift("Christine");
console.log(students)

// What array method does the opposite of push()?
students.pop();
console.log(students);

// What array method does the opposite of unshift?
students.shift();
console.log(students)

// What is the difference between splice() and slice()?
// .splice() allows us to remove and add items from the starting index. (Mutator)
// .slice() copies a portion from a starting index and returns a new array (Non Mutator)

// What is another kind of array method?
// Iterator methods loop over the items of an array

// forEach() - loops over items in an array and repeats a user-defined function.
// map() - loops over items in an array and repeats a user-defined function and returns a new array

// every() - loops and checks if all items satisfy a given condition and returns a boolean. 

let arrNum = [15,25,5,20,10,11];

// check if every item in arrNum is divisible by 5:
let allDivisible;
arrNum.forEach(num => {

    if(num % 5 === 0){
        console.log(`${num} is divisible by 5.`)
    } else {
        allDivisible = false
    }

})

console.log(allDivisible)

// arrNum.pop();
// arrNum.push(35);
let divisibleBy5 = arrNum.every(num => {

    // Every is able to return data.
    // When the function inside every() is able to return true, the loop will continue until all items are able to return true. Else, IF at least one item returns false, then the loop will stop there and ever() will return false

    console.log(num);
    return num % 5 === 0;
})

console.log(divisibleBy5);

// .some()- loops and checks if at least one item satisfies a given condition and returns a boolean. It will return true if at least ONE item satisfies the condition, else if no item returns true/ satisfies the condition, some() will return false.

let someDivisibleBy5 = arrNum.some(num => {

    // .some() will stop its loop once a function/item is able to return true.
    // Then, some() will return true else, if all items do not satisfy the condition or return true, some() will return false()

    console.log(num);
    return num % 5 === 0;
})

console.log(someDivisibleBy5)

arrNum.push(35)
let someDivisibleBy6 = arrNum.some(num => {

    console.log(num);
    return num % 6 === 0;

})

console.log(someDivisibleBy6)

// QUIZ
/* 
    1. Using array literal []
    2. arrayName[0]
    3. arrayName[arrayName.length -1]
    4. indexOf()
    5. forEach()
    6. map()
    7. every()
    8. some()
    9. False
    10. True
    11. False
    12. Math
    13. True
    14. False
    15. True

*/

// FUNCTION CODING
// 1.
let arrName = ["John","Joe","Jane","Jessie","Ryan"];
const addToEnd = (arr,student) => {

    if(typeof(student) === 'string'){
        arr.push(student)
        return arr
    } else {
        return "error - can only add strings to an array";
    }

}

// 2.

const addToStart =(arr,string) => {

    if(typeof(string) === 'string'){
        arr.unshift(string);
        return arr
        
    } else {
        return "error - can only add strings to an array";
    }
}

// 3.

const elementChecker = (arr,student) => {

    if(arr.length === 0){
        return "error - passed in array is empty"
    } else {
        return arr.includes("Jane")
    }

}

// 4. 
const stringLengthSorter = (arr) => {

    let checkString = arr.every(name => {
        return typeof(name) === 'string'
    });

    if(checkString === false){
        return "error - all array elements must be strings"
    } else {

        return arr.sort((a,b) => 
            a.length - b.length

        )
    }


}
